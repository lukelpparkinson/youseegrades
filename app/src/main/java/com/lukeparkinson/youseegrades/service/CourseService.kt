package com.lukeparkinson.youseegrades.service

import com.lukeparkinson.youseegrades.model.Assessment

/**
 * Provides functions for calculations based on a list of assessments
 */
class CourseService(private val assessments: List<Assessment>) {

    /**
     * Gets the maximum achievable grade as a Double if all remaining assessments are completed perfectly
     * @return the max grade as a percentage eg. 97.6
     */
    fun maxGrade(): Double = assessments.sumByDouble {
        it.grade?.times(it.percentage)?.div(100) ?: it.percentage.toDouble()
    }

    /**
     * Gets the current grade from all completed assignments of the course
     * @return the mean of all completed assessments of the course
     */
    fun currentGrade(): Double {
        val assessments = assessments
        val total: Double = assessments.sumByDouble {
            it.grade?.times(it.percentage.toDouble() / 100) ?: 0.0
        }
        val percentageComplete = percentageComplete()
        if (percentageComplete == 0)
            return 0.0
        return (total / percentageComplete) * 100
    }

    /**
     * Gets the lowest achievable grade as a Double if no more assessments are completed
     * @return the min grade as a percentage eg. 53.2
     */
    fun minGrade(): Double = assessments.sumByDouble {
        it.grade?.times(it.percentage.toDouble() / 100) ?: 0.0
    }

    /**
     * Gets the percentage of the course completed
     * @return the amount of the course complete as a percentage eg. 25
     */
    fun percentageComplete(): Int = assessments.sumBy {
        if (it.grade != null) it.percentage else 0
    }
}
