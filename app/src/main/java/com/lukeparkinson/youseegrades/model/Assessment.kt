package com.lukeparkinson.youseegrades.model

import androidx.room.*

/**
 * One assessable item.
 *
 * @property name The name of the assessment
 * @property isFinal True if the assessment is a final exam
 * @property percentage The percentage of the course this assessment accounts for eg. 33
 * @property grade The percentage score achieved in this assessment eg. 98.6
 * @property course The id of the course that this assessment belongs to
 */
@Entity(
    tableName = "assessments",
    foreignKeys = [
        ForeignKey(
            entity = Course::class,
            parentColumns = ["id"],
            childColumns = ["course"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("course")
    ]
)
data class Assessment(
    val course: Long,
    val name: String,
    @ColumnInfo(name = "is_final") val isFinal: Boolean,
    val percentage: Int,
    val grade: Double?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
}