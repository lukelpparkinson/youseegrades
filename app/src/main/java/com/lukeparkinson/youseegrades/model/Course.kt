package com.lukeparkinson.youseegrades.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * One university course eg. SENG365
 *
 * @property code The course code eg. SENG365
 * @property title The course title eg. Web Computing Architectures
 * @property points The number of points the course is worth eg. 15 points
 */
@Entity(
    tableName = "courses",
    indices = [Index("id"), Index("code"), Index("title")]
)
data class Course(
    val code: String,
    val title: String,
    val points: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
}