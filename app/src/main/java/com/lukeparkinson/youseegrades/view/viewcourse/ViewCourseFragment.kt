package com.lukeparkinson.youseegrades.view.viewcourse


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.lukeparkinson.youseegrades.R
import com.lukeparkinson.youseegrades.databinding.FragmentViewCourseBinding

/**
 * A simple [Fragment] subclass.
 *
 */
class ViewCourseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentViewCourseBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_view_course, container, false)
        return binding.root
    }


}
