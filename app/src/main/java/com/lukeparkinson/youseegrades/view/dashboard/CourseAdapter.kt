package com.lukeparkinson.youseegrades.view.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lukeparkinson.youseegrades.R
import com.lukeparkinson.youseegrades.model.Course
import kotlinx.android.synthetic.main.list_item_course.view.*

class CourseAdapter : RecyclerView.Adapter<CourseAdapter.ViewHolder>() {
    var data = listOf<Course>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Course) {
            val res = itemView.context.resources
            itemView.apply {
                list_course_code.text = item.code
                list_course_title.text = item.title
                list_course_points.text = res.getString(R.string.number_points, item.points)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.list_item_course, parent, false)
                return ViewHolder(view)
            }
        }
    }
}