package com.lukeparkinson.youseegrades.view.addcourse

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lukeparkinson.youseegrades.database.CourseDatabaseDao
import com.lukeparkinson.youseegrades.model.Course
import kotlinx.coroutines.*

/**
 * [AndroidViewModel] for adding a [Course] to the database
 */
class AddCourseViewModel(
    private val dataSource: CourseDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    val courseTitle = MutableLiveData<String>()
    val courseCode = MutableLiveData<String>()
    val coursePoints = MutableLiveData<String>()

    private val _eventAddCourse = MutableLiveData<Boolean>()
    val eventAddCourse: LiveData<Boolean>
        get() = _eventAddCourse


    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * When the user taps the add course button add the course and trigger event
     */
    fun onAddCourse() {
        try {
            val course = createCourse()
            uiScope.launch {
                insert(course)
            }
            _eventAddCourse.value = true
        } catch (e: KotlinNullPointerException) {
            Toast.makeText(getApplication(), "mate you didnt do all the form", Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Reset add course event
     */
    fun onAddCourseComplete() {
        _eventAddCourse.value = false
    }


    /**
     * Inserts a course into the database using [Dispatchers.IO]
     */
    private suspend fun insert(course: Course) {
        withContext(Dispatchers.IO) {
            dataSource.insert(course)
        }
        Toast.makeText(getApplication(), "New course added!", Toast.LENGTH_SHORT).show()
    }

    /**
     * Constructs a course from the supplied [MutableLiveData]
     * @throws KotlinNullPointerException if any of the given values have not been changed
     */
    private fun createCourse(): Course {
        return Course(courseCode.value!!, courseTitle.value!!, coursePoints.value!!.toInt())
    }

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     * It is useful when ViewModel observes some data and you need to clear this subscription to prevent a leak of this ViewModel
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}