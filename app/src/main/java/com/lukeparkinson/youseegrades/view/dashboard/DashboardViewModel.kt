package com.lukeparkinson.youseegrades.view.dashboard

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.lukeparkinson.youseegrades.database.CourseDatabaseDao

/**
 * [AndroidViewModel] for viewing courses
 */
class DashboardViewModel(
    courseDao: CourseDatabaseDao,
    application: Application
) : AndroidViewModel(application) {
    val courses = courseDao.getAll()
}