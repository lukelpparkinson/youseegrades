package com.lukeparkinson.youseegrades.view.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.lukeparkinson.youseegrades.R
import com.lukeparkinson.youseegrades.database.CourseDatabase
import com.lukeparkinson.youseegrades.databinding.FragmentDashboardBinding

/**
 * A simple [Fragment] subclass.
 *
 */
class DashboardFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDashboardBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)

        val application = requireNotNull(this.activity).application

        val dataSource = CourseDatabase.getInstance(application).courseDao

        val viewModelFactory = DashboardViewModelFactory(dataSource, application)

        val viewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(DashboardViewModel::class.java)

        binding.dashboardViewModel = viewModel

        val adapter = CourseAdapter()
        binding.courseList.adapter = adapter

        viewModel.courses.observe(viewLifecycleOwner, Observer {
            it?.let { list ->
                adapter.data = list
            }
        })

        binding.lifecycleOwner = this

        binding.addCourseButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_dashboardFragment_to_addCourseFragment)
        }

        return binding.root
    }


}
