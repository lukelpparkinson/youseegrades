package com.lukeparkinson.youseegrades.view.addcourse

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lukeparkinson.youseegrades.database.CourseDatabaseDao

/**
 * Factory to provide an [AddCourseViewModel]
 */
class AddCourseViewModelFactory(
    private val dataSource: CourseDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddCourseViewModel::class.java)) {
            return AddCourseViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}