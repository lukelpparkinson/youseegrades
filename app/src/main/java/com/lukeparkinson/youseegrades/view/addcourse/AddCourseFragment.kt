package com.lukeparkinson.youseegrades.view.addcourse

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.lukeparkinson.youseegrades.R
import com.lukeparkinson.youseegrades.database.CourseDatabase
import com.lukeparkinson.youseegrades.databinding.FragmentAddCourseBinding

/**
 * [Fragment] to deal with adding a new course to the database
 */
class AddCourseFragment : Fragment() {
    private lateinit var binding: FragmentAddCourseBinding
    private lateinit var viewModel: AddCourseViewModel

    /**
     * Sets up the fragment
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_course, container, false)
        val application = requireNotNull(this.activity).application

        val dataSource = CourseDatabase.getInstance(application).courseDao

        val viewModelFactory = AddCourseViewModelFactory(dataSource, application)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddCourseViewModel::class.java)
        viewModel.coursePoints.value = "15"

        binding.addCourseViewModel = viewModel
        binding.lifecycleOwner = this

        addCourseTitleDoneListener()
        addCourseAddedListener()

        return binding.root
    }

    /**
     * If user taps next on the course title EditText then switch to the spinner and hide keyboard
     */
    private fun addCourseTitleDoneListener() {
        binding.courseTitle.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as (InputMethodManager)
                imm.hideSoftInputFromWindow(view.windowToken, 0)
                binding.coursePoints.performClick()
                true
            } else false
        }
    }

    /**
     * Adds a listener for the addCourseEvent
     * When event occurs navigate up
     */
    private fun addCourseAddedListener() {
        viewModel.eventAddCourse.observe(this, Observer { addCourse ->
            if (addCourse) {
                findNavController().navigateUp()
                viewModel.onAddCourseComplete()
            }
        })
    }

}
