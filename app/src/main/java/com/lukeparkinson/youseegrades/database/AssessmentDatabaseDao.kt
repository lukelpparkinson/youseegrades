package com.lukeparkinson.youseegrades.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.lukeparkinson.youseegrades.model.Assessment

/**
 * Handles the database access for [Assessment]
 */
@Dao interface AssessmentDatabaseDao : DataAccessible<Assessment> {

    /**
     * Finds all [Assessment] that belong to the Course
     * @param courseId The id of the Course to query for
     * @return [LiveData] containing all [Assessment] that belong to the given Course
     */
    @Query("SELECT * FROM assessments WHERE course = :courseId")
    fun findAssessmentsByCourse(courseId: Long): LiveData<List<Assessment>>

    /**
     * Inserts one [Assessment] into the database
     * @param model The [Assessment] to be inserted
     * @return the id of the inserted [Assessment]
     */
    @Insert override fun insert(model: Assessment): Long

    /**
     * Updates the given [Assessment] in the database
     * @param model The [Assessment] to be updated
     */
    @Update override fun update(model: Assessment)

    /**
     * Deletes one [Assessment] from the database
     * @param model The [Assessment] to be deleted
     */
    @Delete override fun delete(model: Assessment)

    /**
     * Finds the [Assessment] with the given [id] if it exists, otherwise null
     * @param id The id of the [Assessment]
     * @return the [Assessment] with the matching [id]
     */
    @Query("SELECT * FROM assessments WHERE id = :id")
    override fun get(id: Long): Assessment?

    /**
     * Finds all [Assessment] in the database
     * @return the [List] of all [Assessment]
     */
    @Query("SELECT * FROM assessments ORDER BY id")
    override fun getAll(): LiveData<List<Assessment>>

    /**
     * Deletes all [Assessment] from the database
     */
    @Query("DELETE FROM assessments")
    override fun clear()
}