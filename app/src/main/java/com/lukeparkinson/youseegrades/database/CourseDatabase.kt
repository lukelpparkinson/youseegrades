package com.lukeparkinson.youseegrades.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lukeparkinson.youseegrades.model.Assessment
import com.lukeparkinson.youseegrades.model.Course

@Database(entities = [Course::class, Assessment::class], version = 1, exportSchema = false)
abstract class CourseDatabase : RoomDatabase() {

    abstract val courseDao: CourseDatabaseDao
    abstract val assessmentDao: AssessmentDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: CourseDatabase? = null

        /**
         * Gets the database instance by lazy initialization
         */
        fun getInstance(context: Context): CourseDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CourseDatabase::class.java,
                        "courses_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}