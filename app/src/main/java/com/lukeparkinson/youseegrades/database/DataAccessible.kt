package com.lukeparkinson.youseegrades.database

import androidx.lifecycle.LiveData

/**
 * Interface to ensure all dao have the same basic functionality
 */
interface DataAccessible<M> {

    /**
     * Inserts one [M] into the database
     * @param model The [M] to be inserted
     * @return the id of the inserted [M]
     */
    fun insert(model: M): Long

    /**
     * Updates the given [M] in the database
     * @param model The [M] to be updated
     */
    fun update(model: M)

    /**
     * Deletes one [M] from the database
     * @param model The [M] to be deleted
     */
    fun delete(model: M)

    /**
     * Finds the [M] with the given [id] if it exists, otherwise null
     * @param id The id of the [M]
     * @return the [M] with the matching [id]
     */
    fun get(id: Long): M?

    /**
     * Finds all [M] in the database
     * @return the [List] of all [M]
     */
    fun getAll(): LiveData<List<M>>

    /**
     * Deletes all [M] from the database
     */
    fun clear()
}