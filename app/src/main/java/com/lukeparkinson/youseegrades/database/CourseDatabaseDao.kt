package com.lukeparkinson.youseegrades.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.lukeparkinson.youseegrades.model.Course

/**
 * Handles the database access for [Course]
 */
@Dao interface CourseDatabaseDao : DataAccessible<Course> {

    /**
     * Inserts one [Course] into the database
     * @param model The [Course] to be inserted
     * @return the id of the inserted [Course]
     */
    @Insert override fun insert(model: Course): Long

    /**
     * Updates the given [Course] in the database
     * @param model The [Course] to be updated
     */
    @Update override fun update(model: Course)

    /**
     * Deletes one [Course] from the database
     * @param model The [Course] to be deleted
     */
    @Delete override fun delete(model: Course)

    /**
     * Finds the [Course] with the given [id] if it exists, otherwise null
     * @param id The id of the [Course]
     * @return the [Course] with the matching [id]
     */
    @Query("SELECT * FROM courses WHERE id = :id")
    override fun get(id: Long): Course?

    /**
     * Finds all [Course] in the database
     * @return the [List] of all [Course]
     */
    @Query("SELECT * FROM courses ORDER BY id DESC")
    override fun getAll(): LiveData<List<Course>>

    /**
     * Deletes all [Course] from the database
     */
    @Query("DELETE FROM courses")
    override fun clear()
}