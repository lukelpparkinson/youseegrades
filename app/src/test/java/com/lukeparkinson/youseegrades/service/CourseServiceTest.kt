package com.lukeparkinson.youseegrades.service

import com.lukeparkinson.youseegrades.model.Assessment
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CourseServiceTest {

    private val assessments: List<Assessment>
    private val courseService: CourseService

    init {
        assessments = ArrayList<Assessment>().apply {
            add(Assessment(1, "node", false, 33, 90.0))
            add(Assessment(1, "vue", false, 33, 100.0))
            add(Assessment(1, "exam", true, 33, null))
        }
        courseService = CourseService(assessments)
    }

    @Test
    fun minGrade() = assertEquals(62.7, courseService.minGrade())

    @Test
    fun maxGrade() = assertEquals(95.7, courseService.maxGrade(), 1e-5)

    @Test
    fun percentageComplete() = assertEquals(66, courseService.percentageComplete())

    @Test
    fun currentGrade() = assertEquals(95.0, courseService.currentGrade(), 1e-5)
}
