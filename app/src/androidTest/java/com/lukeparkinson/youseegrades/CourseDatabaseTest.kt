package com.lukeparkinson.youseegrades

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.lukeparkinson.youseegrades.database.AssessmentDatabaseDao
import com.lukeparkinson.youseegrades.database.CourseDatabase
import com.lukeparkinson.youseegrades.database.CourseDatabaseDao
import com.lukeparkinson.youseegrades.model.Course
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class CourseDatabaseTest {

    private lateinit var db: CourseDatabase
    private lateinit var courseDao: CourseDatabaseDao
    private lateinit var assessmentDao: AssessmentDatabaseDao
    @Rule @JvmField val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, CourseDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        courseDao = db.courseDao
        assessmentDao = db.assessmentDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetCourse() {
        val course = Course("SENG365", "Web Computing Architectures", 15)
        val id = courseDao.insert(course)
        val retrieved = courseDao.get(id)
        assertEquals(course, retrieved)
    }

    @Test
    fun insertAndGetAllCourses() {
        val course = Course("SENG365", "Web Computing Architectures", 15)
        courseDao.insert(course)
        assertEquals(1, courseDao.getAll().blockingObserve()!!.size)
    }

    private fun <T> LiveData<T>.blockingObserve(): T? {
        var value: T? = null
        val latch = CountDownLatch(1)

        val observer = Observer<T> { t ->
            value = t
            latch.countDown()
        }

        observeForever(observer)

        latch.await(2, TimeUnit.SECONDS)
        return value
    }
}